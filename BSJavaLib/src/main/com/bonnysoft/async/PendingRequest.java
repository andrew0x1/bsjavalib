/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.async;

/**
 * Implementation IPendingRequest interface.
 * 
 * @author andrew0x1@gmail.com
 */
public class PendingRequest<T, P> extends Task<T> implements IPendingRequest<T, P>, IPendingResponce<T>
{
	/** Listener */
	private ICancelledListener _listener = null;
	/** Cancelled flag. */
	private boolean _isCancelled = false;
	/** Parameters of the request. */
	private final P _parameter;
	
	/**
	 * Constructor.
	 * @param parameters
	 */
	public PendingRequest(P parameter)
	{
		_parameter = parameter;
	}
	
	@Override
	public void setCancelledListener(ICancelledListener listener)
	{
		if(isComplete())
		{
			if((listener != null) && _isCancelled)
				listener.onCancel();
		}
		else
			_listener = listener;
	}

	@Override
	public boolean isCancelled()
	{
		return _isCancelled;
	}
	
	@Override
	public void cancel()
	{
		super.cancel();
		
		if(!_isCancelled)
		{
			if(_listener != null)
				_listener.onCancel();
			_listener = null;
			_isCancelled = true;	
		}
	}

	@Override
	public P getParameter()
	{
		return _parameter;
	}
}
