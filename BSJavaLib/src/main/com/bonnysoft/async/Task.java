/**
 * Copyright (C) 2013 Andrey Yashkov
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.bonnysoft.async;

import com.bonnysoft.common.ICallback;
import com.bonnysoft.common.IResult;
import com.bonnysoft.common.Result;

/**
 * Implementation of the ITask interface. This implementation is not
 * multithreaded.
 * 
 * @param <T> the result type.
 * @author andrew0x1@gmail.com
 */
public class Task<T> implements ITask<T>, ITaskResult<T>, IResult<T, TaskException>
{
	/** Result. */
	private Result<T, TaskException> _result = null;

	/** Callback object. */
	private ICallback<IResult<T, TaskException>> _callback = null;
	
	/** Observer */
	private IProgressListener _progress_listener = null;

	/** Percentage of the task. */
	private float _progress;

	
	/**
	 * Constructor by default.
	 */
	public Task()
	{
		_result = new Result<T, TaskException>();
	}
	
	/**
	 * Constructor.
	 * 
	 * @param result - result of the task.
	 */
	public Task(T result)
	{
		_result = new Result<T, TaskException>(result);
		_progress = 1.0f;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param error - error of the task.
	 */
	public Task(TaskException error)
	{
		_result = new Result<T, TaskException>(error);
	}

	/**
	 * Constructor.
	 * 
	 * @param callback - callback object.
	 */
	public Task(ICallback<IResult<T, TaskException>> callback)
	{
		_callback = callback;
	}

	@Override
	public T getResult() throws TaskException
	{
		return _result.getResult();
	}

	@Override
	public void checkError() throws TaskException
	{
		_result.checkError();
	}
	
	@Override
	public boolean isComplete()
	{
		return _result.isComplete();
	}

	@Override
	public void setCallback(ICallback<IResult<T, TaskException>> callback)
	{
		if(_result.isComplete())
			callback.onComplete(this);
		else
			_callback = callback;
	}

	@Override
	public float getProgress()
	{
		return _progress;
	}

	@Override
	public void setProgressListener(IProgressListener listener)
	{
		if(isComplete() && (listener != null))
			listener.onProgressChanged(_progress);
		else
		{
			_progress_listener = listener;
			if(_progress_listener != null)
				_progress_listener.onProgressChanged(_progress);
		}
	}

	@Override
	public void setResult(T result)
	{
		if(isComplete())
			return;
		
		_result.setResult(result);

		setProgress(1.0f);
		_progress_listener = null;

		if(_callback != null)
			_callback.onComplete(this);

		_callback = null;
		onComplete();
	}

	@Override
	public void setError(TaskException ex)
	{
		if(isComplete())
			return;
		
		_result.setError(ex);

		_progress_listener = null;

		if(_callback != null)
			_callback.onComplete(this);

		_callback = null;
		onComplete();
	}

	@Override
	public void setProgress(float progress)
	{
		if(isComplete())
			return;
		
		_progress = (progress < 0.0f) ? 0.0f : (progress > 1.0f) ? 1.0f : progress;
		if(_progress_listener != null)
			_progress_listener.onProgressChanged(_progress);
	}
	
	@Override
	public void cancel()
	{
		if(!isComplete())
			setError(new TaskCanceledException());
	}

	/**
	 * Called when task is completed.
	 */
	protected void onComplete(){}
}
