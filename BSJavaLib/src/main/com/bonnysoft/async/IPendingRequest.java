/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.async;

/**
 * Interface of the pending request.
 * 
 * @param T - type of the response.
 * @param P - type of the request parameter.
 * @author andrew0x1@gmail.com
 */
public interface IPendingRequest<T, P> extends ITaskResult<T> 
{
	/**
	 * Interface of the observer to the requests state.
	 * 
	 * @author andrew0x1@gmail.com
	 */
	public static interface ICancelledListener
	{
		/**
		 * Called if the request was cancelled.
		 */
		void onCancel();
	}

	/**
	 * Set a listener.
	 * 
	 * @param listener - listener instance.
	 */
	void setCancelledListener(ICancelledListener listener);

	/**
	 * Returns a state of the request.
	 * 
	 * @return true if request was cancelled, false otherwise.
	 */
	boolean isCancelled();
	
	/**
	 * Returns parameters of the request.
	 * 
	 * @return - parameter of the request.
	 */
	 P getParameter();
}
