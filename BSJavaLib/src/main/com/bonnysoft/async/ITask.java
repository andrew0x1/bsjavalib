/**
 * Copyright (C) 2013 Andrey Yashkov
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.bonnysoft.async;

import com.bonnysoft.common.ICallback;
import com.bonnysoft.common.IResult;

/**
 * Interface of task.
 * 
 * @param <T> - type of result.
 * 
 * @author andrew0x1@gmail.com
 */
public interface ITask<T>
{
	/**
	 * Interface for observer of progress.
	 * 
	 * @author andrew0x1@gmail.com
	 * 
	 */
	public static interface IProgressListener
	{
		/**
		 * Called when progress of the task is changed.
		 * 
		 * @param progress value from 0 to 1. 0 - 0% of completion, 1 - 100% of
		 *            completion.
		 */
		void onProgressChanged(float progress);
	}
	
	/**
	 * Break task. Sets TaskCanceledException as result of the task.
	 */
	void cancel();

	/**
	 * Returns flag of completion.
	 * 
	 * @return true if task is completed, false otherwise.
	 */
	boolean isComplete();

	/**
	 * Sets observer to progress of task completion.
	 * 
	 * @param listener - observer.
	 */
	void setProgressListener(IProgressListener listener);

	/**
	 * Sets callback object for result.
	 * 
	 * @param callback - callback object.
	 */
	void setCallback(ICallback<IResult<T, TaskException>> callback);

	/**
	 * Returns the percentage of the task.
	 * 
	 * @return progress value from 0 to 1. 0 - 0% of completion, 1 - 100% of
	 *         completion.
	 */
	float getProgress();
}
