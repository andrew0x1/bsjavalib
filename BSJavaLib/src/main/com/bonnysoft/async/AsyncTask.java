/**
 * Copyright (C) 2013 Andrey Yashkov
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.bonnysoft.async;

import com.bonnysoft.common.IDispatcher;

/**
 * Implementation of the ITask interface. This implementation executes a work in
 * the separate thread.
 * 
 * @param <T> the result type.
 * @author andrew0x1@gmail.com
 */
public abstract class AsyncTask<T> extends Task<T>
{
	/** Dispatcher for result */
	private final IDispatcher _dispatcher;

	/** Percentage of the task. */
	private volatile float _progress;

	/** Interrupt flag */
	protected volatile boolean isCancelled = false;

	/** Dispatch the progress */
	private final Runnable _dispatch_progress = new Runnable()
	{
		@Override
		public void run()
		{
			onProgressUpdate(_progress);
			setProgress(_progress);
		}
	};

	/** The thread for the main work. */
	private final Thread _t = new Thread()
	{
		@Override
		public void run()
		{
			try
			{
				final T result = doInBackground();
				_dispatcher.dispatch(new Runnable()
				{
					@Override
					public void run()
					{
						if(!isComplete())
						{
							onComplete(result);
							setResult(result);
						}
						else if(isCancelled)
							onCancelled(result);
					}
				});
			}
			catch(final TaskException ex)
			{
				_dispatcher.dispatch(new Runnable()
				{
					@Override
					public void run()
					{
						if(!isComplete())
						{
							onFailed(ex);
							setError(ex);
						}
					}
				});
			}
		}
	};

	/**
	 * Constructor.
	 * 
	 * @param dispatcher - the dispatcher from the main thread.
	 */
	public AsyncTask(IDispatcher dispatcher)
	{
		if(dispatcher == null)
			throw new NullPointerException("Parameter 'dispatcher' can't be null.");

		_dispatcher = dispatcher;
		_t.setDaemon(true);
	}

	/**
	 * Executes this task in the separate thread.
	 */
	public final void execute()
	{
		if(!isComplete())
		{
			onPreExecute();
			_t.start();
		}
	}

	/**
	 * Break task.
	 */
	@Override
	public final void cancel()
	{
		if(!isComplete())
		{
			isCancelled = true;
			setError(new TaskCanceledException());
		}
	}

	/**
	 * Called from the separate thread for change the progress.
	 * 
	 * @param progress - the new progress value.
	 */
	protected final void publishProgress(int progress)
	{
		_progress = progress;
		_dispatcher.dispatch(_dispatch_progress);
	}

	/**
	 * Called from the separate thread. All the hard work should to be done
	 * here. You can use isCancelled flag as a signal for the interruption of
	 * the task execution.
	 * 
	 * @return the result of work.
	 * @throws Throwable - if an error is occurred.
	 */
	protected abstract T doInBackground() throws TaskException;

	/**
	 * Called before doInBackground() in the main thread.
	 */
	protected void onPreExecute()
	{
	}
	
	/**
	 * Called when this task is successfully completed.
	 * 
	 * @param result - the result of task.
	 */
	protected void onComplete(T result)
	{
	};

	/**
	 * Called when this task is completed with an error.
	 * 
	 * @param ex - the error descriptor.
	 */
	protected void onFailed(Throwable ex)
	{
	};

	/**
	 * Called when this task is cancelled.
	 * 
	 * @param result - the result of task.
	 */
	protected void onCancelled(T result)
	{
	}
	
	/**
	 * Called when the value of the task progress is changed.
	 * 
	 * @param progress - the value of the task progress.
	 */
	protected void onProgressUpdate(float progress)
	{
	}
}
