/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.async;

/**
 * Base exception type for the async operations.
 * 
 * @author andrew0x1@gmail.com
 */
public class TaskException extends Exception
{
	/***/
	private static final long serialVersionUID = -6922852973517142510L;
	
	/**
	 * Constructor.
	 */
	public TaskException()
	{
		super();
	}

	/**
	 * Constructor.
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public TaskException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Constructor.
	 * @param message
	 * @param cause
	 */
	public TaskException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * Constructor.
	 * @param message
	 */
	public TaskException(String message)
	{
		super(message);
	}

	/**
	 * Constructor.
	 * @param cause
	 */
	public TaskException(Throwable cause)
	{
		super(cause);
	}
}
