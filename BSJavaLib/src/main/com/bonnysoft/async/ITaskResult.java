/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.async;

/**
 * Interface of the task result setter. 
 * 
 * @author andrew0x1@gmail.com
 */
public interface ITaskResult<T>
{
	/**
	 * Sets the result of task. This method cannot be called for the completed
	 * tasks.
	 * 
	 * @param result - result of task.
	 * @exception IllegalStateException - if method is called for completed
	 *                task.
	 */
	void setResult(T result);

	/**
	 * Sets the error result of task. This method cannot be called for the
	 * completed tasks.
	 * 
	 * @param ex - error result of task. This parameter can't be null.
	 * @exception IllegalStateException - if method is called for completed
	 *                task.
	 */
	void setError(TaskException ex);

	/**
	 * Sets percentage of the task.
	 * 
	 * @param progress - progress value from 0 to 1. 0 - 0% of completion, 1 -
	 *            100% of completion.
	 */
	void setProgress(float progress);
}
