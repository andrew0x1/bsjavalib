/**
 * Copyright (C) 2013 Andrey Yashkov
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.bonnysoft.common;

/**
 * Interface of the action.
 * 
 * @param <T> - return type of the action.
 * @param <V> - parameter type of the action.
 * 
 * @author andrew0x1@gmail.com
 */
public interface IAction<T, V>
{
	/**
	 * States of the action.
	 * 
	 * @author andrew0x1@gmail.com
	 */
	public static enum EActionState
	{
		/** Action is enabled. */
		enable,
		/** Action is disabled. */
		disable,
		/** Action is executed. */
		executed
	}

	/**
	 * Observer for the action state.
	 * 
	 * @author andrew0x1@gmail.com
	 */
	public interface IActionStateObserver
	{
		/**
		 * Called when the action state is changed.
		 * 
		 * @param state - state of the action.
		 */
		void onActionStateChanged(IAction<?, ?> source, EActionState state);
	}
	
	/**
	 * Adds the observer for the action state.
	 * 
	 * @param observer - observer for the action state. Can't be null.
	 * 
	 * @return the own parameter.
	 */
	IActionStateObserver registerStateObserver(IActionStateObserver observer);

	/**
	 * Remove the observer of the action state.
	 * 
	 * @param observer - removed observer.
	 */
	void unregisterStateObserver(IActionStateObserver observer);
	
	/**
	 * Set callback for the action. Sets the callback for the action. 
	 * If the action have a result, then the callback will be called 
	 * immediately and the result will be cleared.
	 * 
	 * @param callback - callback for the action. Should not be null.
	 * @return the own parameter.
	 */
	ICallback<IResult<T, ActionException>> setCallback(ICallback<IResult<T, ActionException>> callback);
	
	/**
	 * Remove callback from the action.
	 * 
	 * @param callback - the callback that was set by setCallback method.
	 */
	void removeCallback(ICallback<IResult<T, ActionException>> callback);
	
	/**
	 * Returns the state of the action.
	 * 
	 * @return - state of the action.
	 */
	EActionState getState();

	/**
	 * @return true if the action can be executed, false otherwise.
	 */
	boolean isEnabled();
	
	/**
	 * @return true if the action is not available, false otherwise.
	 */
	boolean isDisabled();
	
	/**
	 * @return true if the action is executed, false otherwise.
	 */
	boolean isExecuted();
	
	/**
	 * Cancel the action execution. If the action is executed then ActionCanceledException 
	 * will be returned in result.
	 */
	void cancel();
	
	/**
	 * Execute the action. Call execute(V param) with null argument.
	 * 
	 * @return themselves.
	 */
	IAction<T, V> execute() throws ActionDisabledException, ActionBusyException;

	/**
	 * Execute the action.
	 * 
	 * @param param - parameters of the action.
	 * @return themselves.
	 */
	IAction<T, V> execute(V param) throws ActionDisabledException, ActionBusyException;
}
