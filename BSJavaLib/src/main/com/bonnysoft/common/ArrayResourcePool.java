/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.common;

/**
 * Implementation of ICache interface for static array. 
 * 
 * @author andrew0x1@gmail.com
 */
public class ArrayResourcePool<T> implements IResourcePool<T>
{
	private final IFactory<T> _factory;
	private final Object[] _cache;
	private int _count = 0;
	private final Object _sync = new Object();

	/**
	 * Constructor.
	 * 
	 * @param capacity - capacity of the cache.
	 * @param factory - factory to create new instances.
	 */
	public ArrayResourcePool(int capacity, IFactory<T> factory)
	{
		if(factory == null)
			throw new NullPointerException("Parameter 'factory' can't be null.");

		_cache = new Object[capacity];
		_factory = factory;
	}

	
	@Override
	@SuppressWarnings("unchecked")
	public T obtain()
	{
		synchronized(_sync)
		{
			if(_count > 0)
			{
				final T ret = (T)_cache[--_count];
				_cache[_count] = null;
				return ret;
			}
		}
		
		return _factory.getInstance();
	}

	@Override
	public void recyle(T object)
	{
		if(object == null)
			throw new NullPointerException("Parameter 'object' can't be null.");
		
		synchronized(_sync)
		{
			if(_count < _cache.length)
				_cache[_count++] = object;
		}
	}
}
