/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.common;

import com.bonnysoft.common.IProperty.EPropertyState;
import com.bonnysoft.common.IProperty.IPropertyObserver;

/**
 * 
 * @author andrew0x1@gmail.com
 */
public class PropertyObserver<T> implements IPropertyObserver<T>, IDestroyable
{
	/** The observable property. */
	private IProperty<T> _property;
	
	/**
	 * Constructor.
	 * 
	 * @param property - the observable property.
	 */
	public PropertyObserver(IProperty<T> property)
	{
		if(property == null)
			throw new NullPointerException("Parameter 'property' can't be null.");
		
		_property = property;
		_property.addObserver(this);
	}
	
	@Override
	public boolean isDestroyed()
	{
		return (_property == null);
	}

	@Override
	public void destroy()
	{
		if(_property != null)
		{		
			_property.removeObserver(this);
			_property = null;
		}
	}
	
	@Override
	public void onPropertyStateChanged(IProperty<T> source, EPropertyState state){}

	@Override
	public void onPropertyValueChanged(IProperty<T> source, T value){}
}
