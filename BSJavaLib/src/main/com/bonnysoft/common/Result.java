/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.common;

/**
 * Base implementation of the IResult
 * 
 * @param <T> - type of the result.
 * @param <E> - exception type.
 * @author andrew0x1@gmail.com
 */
public class Result<T, E extends Throwable> implements IResult<T, E>
{
	/** Result. */
	private T _result = null;

	/** Error. */
	private E _exception = null;

	/** Flag of completion. */
	private boolean _isComplete = false;
	
	/**
	 * Constructor.
	 */
	public Result()
	{
	}
	
	/**
	 * Constructor.
	 * 
	 * @param result - result.
	 */
	public Result(T result)
	{
		_isComplete = true;
		_result = result;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param result - error.
	 */
	public Result(E error)
	{
		_isComplete = true;
		_exception = error;
	}
	
	@Override
	public T getResult() throws E
	{
		if(_isComplete)
		{
			if(_exception != null)
				throw _exception;

			return _result;
		}

		return null;
	}
	
	@Override
	public void checkError() throws E
	{
		if(_isComplete && (_exception != null))
			throw _exception;
	}
	
	@Override
	public boolean isComplete()
	{
		return _isComplete;
	}
	
	/**
	 * Set error as result.
	 * 
	 * @param ex - exception with error description.
	 */
	public void setError(E ex)
	{
		if(ex == null)
			throw new NullPointerException("Parameter 'ex' can't be null.");

		if(_isComplete)
			throw new IllegalStateException("The second result cannot be set for completed task.");

		_isComplete = true;
		_exception = ex;
	}
	
	/**
	 * Set type instance as result.
	 * 
	 * @param result - result.
	 */
	public void setResult(T result)
	{
		if(_isComplete)
			throw new IllegalStateException("The second result cannot be set for completed task.");

		_isComplete = true;
		_result = result;
	}
	
	/**
	 * Reset state.
	 */
	public void reset()
	{
		_isComplete = false;
		_result = null;
		_exception = null;
	}
}
