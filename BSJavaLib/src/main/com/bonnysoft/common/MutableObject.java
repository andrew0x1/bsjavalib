/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.common;

/**
 * Lets to sends the mutable objects into the inner anonymous classes. 
 * 
 * @author andrew0x1@gmail.com
 */
public class MutableObject<T>
{
	/** Wrapped value. */
	private T _value = null;

	/**
	 * Constructor.
	 */
	public MutableObject(){}
	
	/**
	 * Constructor.
	 * 
	 * @param value - value by default.
	 */
	public MutableObject(T value)
	{
		_value = value;
	}

	/**
	 * Returns the wrapped value.
	 * 
	 * @return wrapped value.
	 */
	public T getValue()
	{
		return _value;
	}

	/**
	 * Sets the wrapped value.
	 * 
	 * @param value - new wrapped value.
	 */
	public void setValue(T value)
	{
		this._value = value;
	}
}
