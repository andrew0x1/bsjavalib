/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.common;

import java.util.HashSet;
import java.util.Set;

/**
 * Base implementation of the IAction interface.
 * 
 * @param <T> - return type of the action.
 * @param <V> - parameter type of the action.
 * 
 * @author andrew0x1@gmail.com
 */
public abstract class AbstractAction<T, V> implements IAction<T, V>
{
	/** Observers. */
	private final Set<IActionStateObserver> _state_observers = new HashSet<IActionStateObserver>(1);
	
	/** Current state of the action. */
	private EActionState _state = EActionState.disable;
	
	/** Last action result. */
	private final Result<T, ActionException> _result = new Result<T, ActionException>();
	
	/** Callback for the action. */
	private ICallback<IResult<T, ActionException>> _callback = null;
	
	/**
	 * Constructor.
	 */
	public AbstractAction()
	{
		_state = EActionState.disable;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param isEnabled - state by default.
	 */
	public AbstractAction(boolean isEnabled)
	{		
		_state = isEnabled ? EActionState.enable : EActionState.disable;
	}
	
	@Override
	public IActionStateObserver registerStateObserver(IActionStateObserver observer)
	{
		if(observer == null)
			throw new NullPointerException("Parameter 'observer' can't be null.");
		
		if(_state_observers.add(observer))
			observer.onActionStateChanged(this, _state);
		
		return observer;
	}

	@Override
	public void unregisterStateObserver(IActionStateObserver observer)
	{
		if(observer != null)
			_state_observers.remove(observer);
	}

	@Override
	public EActionState getState()
	{
		return _state;
	}

	@Override
	public boolean isEnabled()
	{
		return EActionState.enable.equals(_state);
	}

	@Override
	public boolean isDisabled()
	{
		return EActionState.disable.equals(_state);
	}

	@Override
	public boolean isExecuted()
	{
		return EActionState.executed.equals(_state);
	}
	
	@Override
	public ICallback<IResult<T, ActionException>> setCallback(ICallback<IResult<T, ActionException>> callback)
	{
		if(callback == null)
			throw new NullPointerException("Parameter 'callback' can't be null.");
		
		_callback = callback;
		if(_result.isComplete())
		{
			_callback.onComplete(_result);
			_result.reset();
		}

		return _callback;
	}

	@Override
	public void removeCallback(ICallback<IResult<T, ActionException>> callback)
	{
		if(callback == null)
			throw new NullPointerException("Parameter 'callback' can't be null.");
		
		if(_callback == callback)
			_callback = null;
	}

	@Override
	public IAction<T, V> execute() throws ActionDisabledException, ActionBusyException
	{
		return execute(null);
	}

	@Override
	public IAction<T, V> execute(V param) throws ActionDisabledException, ActionBusyException
	{
		if(isDisabled())
			throw new ActionDisabledException();
		if(isExecuted())
			throw new ActionBusyException();
		
		_result.reset();
		_setState(EActionState.executed);
		onExecute(param);

		return this;
	}
	
	/**
	 * Enable the action.
	 */
	public void enable()
	{
		if(isDisabled())
			_setState(EActionState.enable);
	}
	
	/**
	 * Disable the action. If action is executed then action task will
	 * be cancelled.
	 */
	public void disable()
	{
		if(isDisabled())
			return;
		
		if(isExecuted())
		{
			_result.setError(new ActionCanceledException());
			if(_callback != null)
				_callback.onComplete(_result);

			onCanceled();
		}
		
		_result.reset();
		_setState(EActionState.disable);
	}
	
	@Override
	public void cancel()
	{
		if(isExecuted())
		{
			setError(new ActionCanceledException());
			onCanceled();
		}
	}
	
	/**
	 * Sets the result of the action.
	 * 
	 * @param result - result of the action.
	 */
	public void setResult(T result)
	{
		if(isExecuted())
		{
			_result.setResult(result);
			if(_callback != null)
			{
				_callback.onComplete(_result);
				_result.reset();
			}

			_setState(EActionState.enable);
		}
	}
	
	/**
	 * Sets the error of the action.
	 * 
	 * @param error - error of the action.
	 */
	public void setError(ActionException error)
	{
		if(isExecuted())
		{
			_result.setError(error);
			if(_callback != null)
			{
				_callback.onComplete(_result);
				_result.reset();
			}

			_setState(EActionState.enable);
		}
	}
	
	/**
	 * Set state of the action.
	 * @param state - new state.
	 */
	private void _setState(EActionState state)
	{
		if(state == null)
			throw new NullPointerException("Parameter 'state' can't be null.");
		
		if(_state.equals(state))
			return;
		
		_state = state;
		for(IActionStateObserver o : _state_observers)
			o.onActionStateChanged(this, state);
	}
	
	/**
	 * Called when implementation must execute action task.
	 * 
	 * @param param - parameters of the execution.
	 * @return ITask implementation. Must not be null.
	 */
	protected abstract void onExecute(V param);
	
	/**
	 * Called when action is canceled.
	 */
	protected void onCanceled(){};
}
