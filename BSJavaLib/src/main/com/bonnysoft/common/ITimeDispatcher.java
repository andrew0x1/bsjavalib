/**
 * Copyright (C) 2013 Andrey Yashkov
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.bonnysoft.common;

/**
 * Interface for multi-threaded interaction.
 * 
 * @author andrew0x1@gmail.com
 */
public interface ITimeDispatcher extends IDispatcher
{
	/**
	 * Execute 'r' in the dispatcher context with delay.
	 * 
	 * @param delay - the delay of the 'r' execution in milliseconds.
	 * @param r - this parameter can't be null.
	 */
	void dispatchDelay(int delay, Runnable r);
}
