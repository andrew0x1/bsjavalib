/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.common;

/**
 * Interface of result an operation.
 * 
 * @param <T> - type of result.
 * @param <E> - exception type.
 * 
 * @author andrew0x1@gmail.com
 */
public interface IResult<T, E extends Throwable>
{
	/**
	 * Returns result or throws an exception.
	 * 
	 * @return result of task. Method returns null until task is completed.
	 * @throws E - if an error is occurred.
	 */
	T getResult() throws E;
	
	/**
	 * Checks the state of the result. If the result contains an error, 
	 * then the exception will be thrown.
	 * @throws E - if an error is occurred.
	 */
	void checkError() throws E;

	/**
	 * Returns flag of result is set.
	 * 
	 * @return true if result is setup, false otherwise.
	 */
	boolean isComplete();
}