/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.common;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of the IProperty interface.
 * 
 * @author andrew0x1@gmail.com
 */
public class Property<T> implements IProperty<T>
{
	/** Observers. */
	private final Set<IPropertyObserver<T>> _observers = new HashSet<IPropertyObserver<T>>(1);
	
	/** Current state of the action. */
	private EPropertyState _state = EPropertyState.undefined;
	
	/** Value of the property. */
	private T _value = null;
	
	/**
	 * Constructor.
	 */
	public Property()
	{
		_state = EPropertyState.undefined;
		_value = null;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param def - default value for the property.
	 */
	public Property(T def)
	{
		_state = EPropertyState.valid;
		_value = def;
	}
	
	@Override
	public IPropertyObserver<T> addObserver(IPropertyObserver<T> observer)
	{
		if(observer == null)
			throw new NullPointerException("Parameter 'observer' can't be null.");
		
		if(_observers.add(observer))
		{
			observer.onPropertyStateChanged(this, _state);
			if(!EPropertyState.undefined.equals(_state))
				observer.onPropertyValueChanged(this, _value);
			return observer;
		}
		
		return null;
	}

	@Override
	public void removeObserver(IPropertyObserver<T> observer)
	{
		if(observer != null)
			_observers.remove(observer);
	}

	@Override
	public T getValue()
	{
		return (EPropertyState.undefined.equals(_state)) ? null : _value;
	}

	@Override
	public EPropertyState getState()
	{
		return _state;
	}

	@Override
	public boolean isValid()
	{
		return !EPropertyState.undefined.equals(_state);
	}

	/**
	 * Sets the new value for the property and set the state of the property to the valid.
	 * 
	 * @param new_value - new value for the property.
	 */
	public void setValue(T new_value)
	{
		if(!EPropertyState.valid.equals(_state))
		{
			_value = new_value;
			_setState(EPropertyState.valid);
			
			for(IPropertyObserver<T> o : _observers)
				o.onPropertyValueChanged(this, _value);	
		}
		else
		{			
			if(new_value != null)
			{
				if(new_value.equals(_value))
					return;
			}
			else if(_value == null)
				return;
			
			_value = new_value;
		
			for(IPropertyObserver<T> o : _observers)
				o.onPropertyValueChanged(this, _value);	
		}
	}
	
	/**
	 * Erase the value of the property and sets the state of the property to the undefined.
	 */
	public void reset()
	{
		_value = null;
		_setState(EPropertyState.undefined);
	}
	
	/**
	 * Sets the state of the property to the updated.
	 */
	public void markAsUpdated()
	{
		_setState(EPropertyState.updated);
	}
	
	/**
	 * Notify data to the observers if the property in the valid state.
	 */
	public void notifyData()
	{
		if(!EPropertyState.valid.equals(_state))
			return;
		
		for(IPropertyObserver<T> o : _observers)
			o.onPropertyValueChanged(this, _value);
	}
	
	/**
	 * Set state of the action.
	 * @param state - new state.
	 */
	private void _setState(EPropertyState state)
	{
		if((state == null) || _state.equals(state))
			return;
		
		_state = state;
		for(IPropertyObserver<T> o : _observers)
			o.onPropertyStateChanged(this, state);
	}
}
