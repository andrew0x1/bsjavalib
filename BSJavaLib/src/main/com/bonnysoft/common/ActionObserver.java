/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.common;

import com.bonnysoft.common.IAction.EActionState;
import com.bonnysoft.common.IAction.IActionStateObserver;

/**
 * 
 * @author andrew0x1@gmail.com
 */
public class ActionObserver<T> implements IDestroyable, IActionStateObserver, ICallback<IResult<T, ActionException>>
{
	/** Observable action. */
	private IAction<T, ?> _action;
	private EActionState _last_state;
	
	/**
	 * Constructor.
	 * 
	 * @param action - observable action.
	 */
	public ActionObserver(IAction<T, ?> action)
	{
		this(action, false);
	}
	
	/**
	 * Constructor.
	 * 
	 * @param action - observable action.
	 * @param get_result - if true, then IAction.setCallback will be called. 
	 */
	public ActionObserver(IAction<T, ?> action, boolean get_result)
	{
		if(action == null)
			throw new NullPointerException("Parameter 'action' can't be null.");
		
		_action = action;
		_last_state = _action.getState();
		_action.registerStateObserver(this);
		
		if(get_result)
			_action.setCallback(this);
	}
	
	@Override
	public boolean isDestroyed()
	{
		return (_action == null);
	}
	
	/**
	 * @return true if the action can be executed, false otherwise.
	 */
	public boolean actionIsEnabled()
	{
		return EActionState.enable.equals(_last_state);
	}
	
	/**
	 * @return true if the action is not available, false otherwise.
	 */
	public boolean actionIsDisabled()
	{
		return EActionState.disable.equals(_last_state);
	}
	
	/**
	 * @return true if the action is executed, false otherwise.
	 */
	public boolean actionIsExecuted()
	{
		return EActionState.executed.equals(_last_state);
	}
	
	@Override
	public void destroy()
	{
		if(_action != null)
		{		
			_action.unregisterStateObserver(this);
			_action.removeCallback(this);
			_action = null;
			_last_state = EActionState.disable;
		}
	}

	@Override
	public final void onActionStateChanged(IAction<?, ?> source, EActionState state)
	{
		_last_state = state;
		onStateChanged(state);
		if((_action != null) && (source == _action) && EActionState.executed.equals(state))
			_action.setCallback(this);
	}

	@Override
	public final void onComplete(IResult<T, ActionException> data)
	{
		if(_action != null)
			onResult(data);
	}
	
	/**
	 * Called when state of the observable action is changed.
	 * 
	 * @param state - state of the observable action.
	 */
	protected void onStateChanged(EActionState state){}
	
	/**
	 * Called when result from the action is coming.
	 * 
	 * @param data - result of the action.
	 */
	protected void  onResult(IResult<T, ActionException> data){}
}
