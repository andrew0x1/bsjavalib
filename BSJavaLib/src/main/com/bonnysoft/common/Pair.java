/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.common;

/**
 * 
 * @author andrew0x1@gmail.com
 */
public class Pair<F, S>
{
	/** First value of the pair. */
	private F _first;
	/** Second value of the pair. */
	private S _second;
	
	/**
	 * Constructor.
	 */
	public Pair()
	{
		_first = null;
		_second = null;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param first - initialized value for the first value of the pair.
	 * @param second - initialized value for the second value of the pair.
	 */
	public Pair(F first, S second)
	{
		_first = first;
		_second = second;
	}
	
	/**
	 * Sets the first value of the pair.
	 * 
	 * @param first - value.
	 */
	public void setFirst(F first)
	{
		_first = first;
	}

	/**
	 * @return the first value of the pair.
	 */
	public F getFirst()
	{
		return _first;
	}
	
	/**
	 * Sets the second value of the pair.
	 * 
	 * @param second - value.
	 */
	public void setSecond(S second)
	{
		_second = second;
	}
	
	/**
	 * @return the second value of the pair.
	 */
	public S getSecond()
	{
		return _second;
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_first == null) ? 0 : _first.hashCode());
		result = prime * result + ((_second == null) ? 0 : _second.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Pair<?, ?> other = (Pair<?, ?>) obj;
		if(_first == null)
		{
			if(other._first != null)
				return false;
		}
		else if(!_first.equals(other._first))
			return false;
		if(_second == null)
		{
			if(other._second != null)
				return false;
		}
		else if(!_second.equals(other._second))
			return false;
		return true;
	}
}
