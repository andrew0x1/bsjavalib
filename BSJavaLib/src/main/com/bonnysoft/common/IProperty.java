/**
 * Copyright (C) 2013 Andrey Yashkov
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.bonnysoft.common;

/**
 * Interface of the property.
 * 
 * @param <T> - the type of the property value.
 * 
 * @author andrew0x1@gmail.com
 */
public interface IProperty<T>
{
	/**
	 * States of the property.
	 * 
	 * @author andrew0x1@gmail.com
	 */
	public static enum EPropertyState
	{
		/** Property value is valid. */
		valid,

		/** Property value is undefined. */
		undefined,

		/** Property value is updated. */
		updated,
	}

	/**
	 * Interface of the observer for the property.
	 * 
	 * @param <T> - the type of the property value.
	 * @author andrew0x1@gmail.com
	 */
	public static interface IPropertyObserver<T>
	{
		/**
		 * Called when the state of the property is changed.
		 * 
		 * @param state - sate of the property.
		 */
		void onPropertyStateChanged(IProperty<T> source, EPropertyState state);

		/**
		 * Called when the value of the property is changed.
		 * 
		 * @param value - new value of the property.
		 */
		void onPropertyValueChanged(IProperty<T> source, T value);
	}
	
	/**
	 * Adds the observer for the property.
	 * 
	 * @param observer - observer for the property. Can't be null.
	 * 
	 * @return the own parameter.
	 */
	IPropertyObserver<T> addObserver(IPropertyObserver<T> observer);

	/**
	 * Remove the observer of the property.
	 * 
	 * @param observer - removed observer.
	 */
	void removeObserver(IPropertyObserver<T> observer);

	/**
	 * Returns the value of the property.
	 * 
	 * @return the value of the property or null if the property is not in the
	 *         valid state.
	 */
	T getValue();

	/**
	 * @return the state of the property.
	 */
	EPropertyState getState();
	
	/**
	 * @return true if the property in the valid state, false otherwise.
	 */
	boolean isValid();
}
