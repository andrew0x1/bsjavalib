/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.common;

import java.util.ArrayList;
import java.util.List;

/**
 * Base implements of IObservable interface.
 * @author andrew0x1@gmail.com
 *
 * @param <T> - type of observer.
 */
public class BaseObservable<T> implements IObservable<T>
{
	/**
	 * Auxiliary class for safe operations.
	 * 
	 * @param <T> - type of observer.
	 * 
	 * @author andrew0x1@gmail.com
	 */
	private static final class _Operation<T>
	{
		final boolean isAdded;
		final T observer;

		_Operation(boolean isAdded, T observer)
		{
			this.observer = observer;
			this.isAdded = isAdded;
		}
	}

	/** Observers. */
	protected final List<T> _observers;
	/** Safe operations. */
	private final List<_Operation<T>> _operations = new ArrayList<_Operation<T>>();
	/** Flag */
	private boolean _isIterated = false;

	/**
	 * Constructor. By default creates ArrayList of observers.
	 */
	public BaseObservable()
	{
		this(new ArrayList<T>());
	}

	/**
	 * Constructor. Setup user defined container for observers.
	 * 
	 * @param container - container for observers. This parameter can't be null.
	 */
	public BaseObservable(List<T> container)
	{
		if(container == null)
			throw new NullPointerException("Parameter 'container' can't be null.");

		_observers = container;
	}

	@Override
	public void addObserver(T observer)
	{
		if(observer == null)
			return;

		if(_isIterated)
			_operations.add(new _Operation<T>(true, observer));
		else
			_addObserver(observer);
	}

	@Override
	public void removeObserver(T observer)
	{
		if(observer == null)
			return;

		if(_isIterated)
			_operations.add(new _Operation<T>(false, observer));
		else
			_removeObserver(observer);
	}

	/**
	 * Iterate all observers.
	 * 
	 * @param iterator - iterate all observers.
	 */
	public void iterateAll(IIterator<T> iterator)
	{
		if(iterator == null)
			return;

		_isIterated = true;
		try
		{
			for(T item : _observers)
			{
				if(!iterator.onItem(item))
					break;
			}

			_isIterated = false;

			for(_Operation<T> op : _operations)
			{
				if(op.isAdded)
					_addObserver(op.observer);
				else
					_removeObserver(op.observer);
			}
		}
		finally
		{
			_isIterated = false;
			_operations.clear();
		}
	}

	/**
	 * Adds observer to collection.
	 * 
	 * @param observer - observer. This parameter can't be null.
	 */
	private void _addObserver(T observer)
	{
		if(!_observers.contains(observer))
		{
			_observers.add(observer);
			onObserverAdded(observer);
		}
	}

	/**
	 * Removes observer from collection.
	 * 
	 * @param observer - observer. This parameter can't be null.
	 */
	private void _removeObserver(T observer)
	{
		_observers.remove(observer);
		onObserverRemoved(observer);
	}

	/**
	 * Calls for an observer after it is added to the collection.
	 * 
	 * @param observer - added observer, not null.
	 */
	protected void onObserverAdded(T observer)
	{
	}

	/**
	 * Calls for an observer after it is removed from the collection.
	 * 
	 * @param observer - removed observer, not null.
	 */
	protected void onObserverRemoved(T observer)
	{
	}
}