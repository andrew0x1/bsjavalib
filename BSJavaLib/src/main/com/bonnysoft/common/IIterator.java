/**
 * Copyright (C) 2012 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bonnysoft.common;

/**
 * Interface for iterate of collections.
 *
 * @author andrew0x1@gmail.com
 */
public interface IIterator<T>
{
	/**
	 * Called for each item in the collection.
	 * @param item - item in the collection.
	 * @return true to continue iterate, false to stop iterate.
	 */
	boolean onItem(T item);
}
