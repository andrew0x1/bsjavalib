/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.utils;

/**
 * Class for string functions.
 * 
 * @author andrew0x1@gmail.com
 */
public class StringUtils
{
	/** Строка донор для перевода Byte в char */
	private static final String _HEXES = "0123456789ABCDEF";

	/**
	 * Convert a byte array to the HEX string.
	 * 
	 * @param raw - array of bytes.
	 * @return HEX string.
	 */
	public static String Array2HexString(byte[] raw)
	{		
		return Array2HexString(raw, 0, raw.length);
	}

	/**
	 * Convert a byte array to the HEX array string.
	 * 
	 * @param raw - array of bytes.
	 * @param offset - offset from the beginning of the array.
	 * @param len - the number of bytes to convert.
	 * @return HEX string.
	 */
	public static String Array2HexArrayString(byte[] raw)
	{
		return Array2HexArrayString(raw, 0, raw.length);
	}
	
	/**
	 * Convert a byte array to the HEX string.
	 * 
	 * @param raw - array of bytes.
	 * @param offset - offset from the beginning of the array.
	 * @param len - the number of bytes to convert.
	 * @return HEX string.
	 */
	public static String Array2HexString(byte[] raw, int offset, int len)
	{
		if(raw == null)
			return "";

		final StringBuilder hex = new StringBuilder(2 * len);
		final int last = offset + len;
		for(int i = offset; i < last; i++)
		{
			final byte b = raw[i];
			hex.append(_HEXES.charAt((b & 0xF0) >> 4)).append(_HEXES.charAt((b & 0x0F)));
		}

		return hex.toString();
	}
	
	/**
	 * Convert a byte array to the HEX array string.
	 * 
	 * @param raw - array of bytes.
	 * @param offset - offset from the beginning of the array.
	 * @param len - the number of bytes to convert.
	 * @return HEX string.
	 */
	public static String Array2HexArrayString(byte[] raw, int offset, int len)
	{
		if(raw == null)
			return "";
		if(raw.length == 0)
			return "[]";

		final StringBuilder hex = new StringBuilder(2 * len + len + 1);
		final int last = offset + len;
		hex.append('[');
		for(int i = offset; i < last; i++)
		{
			final byte b = raw[i];
			hex.append(_HEXES.charAt((b & 0xF0) >> 4)).append(_HEXES.charAt((b & 0x0F))).append(',');
		}
		hex.setCharAt(hex.length() - 1, ']');

		return hex.toString();
	}
}
