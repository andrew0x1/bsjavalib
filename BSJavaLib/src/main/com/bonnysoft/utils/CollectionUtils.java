/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.utils;

import java.util.Collection;

import com.bonnysoft.common.IDestroyable;

/**
 * Class for collections.
 * 
 * @author andrew0x1@gmail.com
 */
public class CollectionUtils
{
	/**
	 * Calls the IDestroyable.destroy() method for the every object in the collection 
	 * and clear the collection.
	 * 
	 * @param collection - collection instance. Should not be null.
	 */
	public static <T extends IDestroyable> void destroyCollection(Collection<T> collection)
	{
		if(collection == null)
			throw new NullPointerException("Parameter 'collection' can't be null.");
		
		for(T element : collection)
			if(element != null)
				element.destroy();
		
		collection.clear();
	}
}
