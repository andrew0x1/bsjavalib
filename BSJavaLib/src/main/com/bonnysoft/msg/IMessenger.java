/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.msg;

import com.bonnysoft.async.IPendingResponce;

/**
 * Interface of messenger.
 * 
 * @param T - type of argument for the request.
 * @param V - type of argument for the response.
 * @author andrew0x1@gmail.com
 */
public interface IMessenger<T, V>
{
	/**
	 * Interface of the message handler.
	 * 
	 * @param T - type of argument for the request.
	 * @param V - type of argument for the response.
	 * @author andrew0x1@gmail.com
	 */
	public static interface IMessageHandler<T, V>
	{
		/**
		 * Called when a new message is coming.
		 * 
		 * @param message -  the type of the message.
		 * @param args - the parameter of the message.
		 * 
		 * @return true if the message is processed, false otherwise.
		 */
		boolean onMessage(int message, T args);

		/**
		 * Вызывается в момент поступления нового запроса.
		 * 
		 * @param message - тип запроса.
		 * @param args - аргументы запроса.
		 * @param responce - интерфейс для записи ответа на запрос.
		 * 
		 * @return true если сообщения принято в обработку, иначе false.
		 */
		
		/**
		 * Called when a new request is coming.
		 * 
		 * @param message -  the type of the request.
		 * @param args - the parameter of the request.
		 * 
		 * @return true if the request is processed, false otherwise.
		 */
		boolean onRequest(int message, T args, IPendingResponce<V> responce);
	}
	
	/**
	 * Register a new handler for this Messenger.
	 * 
	 * @param handler - handler instance, not null.
	 */
	void registerHandler(IMessageHandler<T, V> handler);

	/**
	 * Unregister a handler from this Messenger.
	 * 
	 * @param handler - handler instance, not null.
	 */
	void unregisterHandler(IMessageHandler<T, V> handler);
	
}
