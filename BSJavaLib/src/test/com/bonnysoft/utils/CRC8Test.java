/**
 * Copyright (C) 2013 Andrey Yashkov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bonnysoft.utils;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author andrew0x1@gmail.com
 */
public class CRC8Test
{

	@Test
	public void test_crc_calculation_for_array()
	{
		byte[] sourse = new byte[]{(byte)0xE1, (byte)0x00, (byte)0xCA, (byte)0xFE, (byte)0x00, (byte)0xBB, (byte)0x58, (byte)0x17};
		assertEquals("Invalid crc calculation.", 0x78, 0xFF & CRC8.getCRC8(sourse, 0, 8));
	}
	
	@Test
	public void test_crc_calculation_for_byte()
	{
		assertEquals("Invalid crc calculation.",  0xF0, 0xFF & CRC8.getCRC8((byte)0xE1));
	}
	
	@Test
	public void test_crc_calculation_for_composite_data()
	{
		byte[] sourse = new byte[]{(byte)0x00, (byte)0xCA, (byte)0xFE, (byte)0x00, (byte)0xBB, (byte)0x58, (byte)0x17};
		assertEquals("Invalid crc calculation.", 0x78, 0xFF & CRC8.getCRC8( CRC8.getCRC8((byte)0xE1), sourse, 0, 7));
	}
}
